from django.shortcuts import (
    get_object_or_404, render, HttpResponseRedirect, redirect)
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from .forms import Add_Promotion, Add_Subject, CustomUserCreationForm
from .models import Promotion, Subject
from django.contrib.auth.models import User
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy


def is_authorized(request, action, model):
    perm = "app."+action+"_"+model
    return request.user.is_authenticated and request.user.has_perm(perm)

# PROMOTION


def add_promotion(request):
    if not is_authorized(request, "add", 'promotion'):
        return redirect('/accounts/login/')
    else:
        context = {}
        form = Add_Promotion(request.POST or None)
        if form.is_valid():
            form.save()
        context['form'] = form
        return render(request, "../templates/form_add.html", context)


def list_promotion(request):
    if not is_authorized(request, "view", 'promotion'):
        return redirect('/accounts/login/')
    else:
        context = {}
        context["classname"] = "promotion"
        context["dataset"] = Promotion.objects.all()
        return render(request, "list_view.html", context)


def detail_promotion(request, id):
    if not is_authorized(request, "view", 'promotion'):
        return redirect('/accounts/login/')
    else:
        context = {}
        context["classname"] = "promotion"
        context["data"] = Promotion.objects.get(id=id)
        return render(request, "detail_view.html", context)


def update_promotion(request, id):
    if not is_authorized(request, "change", 'promotion'):
        return redirect('/accounts/login/')
    else:
        context = {}
        obj = get_object_or_404(Promotion, id=id)
        form = Add_Promotion(request.POST or None, instance=obj)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("../")
        context["form"] = form
        return render(request, "update_view.html", context)


def delete_promotion(request, id):
    if not is_authorized(request, "delete", 'promotion'):
        return redirect('/accounts/login/')
    else:
        context = {}
        obj = get_object_or_404(Promotion, id=id)
        if request.method == "POST":
            obj.delete()
            return HttpResponseRedirect("../")

        return render(request, "delete_view.html", context)

# SUBJECT


def add_subject(request):
    if not is_authorized(request, "add", 'subject'):
        return redirect('/accounts/login/')
    else:
        context = {}
        form = Add_Subject(request.POST or None)
        if form.is_valid():
            form.save()
        context['form'] = form
        return render(request, "../templates/form_add.html", context)


def list_subject(request):
    if not is_authorized(request, "view", 'subject'):
        return redirect('/accounts/login/')
    else:
        context = {}
        context["classname"] = "subject"
        context["dataset"] = Subject.objects.all()
        return render(request, "list_view.html", context)


def detail_subject(request, id):
    if not is_authorized(request, "view", 'subject'):
        return redirect('/accounts/login/')
    else:
        context = {}
        context["data"] = Subject.objects.get(id=id)
        return render(request, "detail_view.html", context)


def update_subject(request, id):
    if not is_authorized(request, "change", 'subject'):
        return redirect('/accounts/login/')
    else:
        context = {}
        obj = get_object_or_404(Subject, id=id)
        form = Add_Subject(request.POST or None, instance=obj)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("../")

        context["form"] = form

    return render(request, "update_view.html", context)


def delete_subject(request, id):
    if not is_authorized(request, "delete", 'subject'):
        return redirect('/accounts/login/')
    else:
        context = {}
        obj = get_object_or_404(Subject, id=id)
        if request.method == "POST":
            obj.delete()
            return HttpResponseRedirect("../")

        return render(request, "delete_view.html", context)


# USER
def add_user(request):
    if not is_authorized(request, "add", 'users'):
        return redirect('/accounts/login/')
    else:
        if request.method == 'POST':
            f = CustomUserCreationForm(request.POST)
            if f.is_valid():
                f.save()
                messages.success(request, 'Account created successfully')
                return redirect('add_user')
        else:
            f = CustomUserCreationForm()
        return render(request, '../templates/form_add.html', {'form': f})


def list_user(request):
    if not is_authorized(request, "view", 'users'):
        return redirect('/accounts/login/')
    else:
        context = {}
        context["classname"] = "user"
        context["dataset"] = User.objects.all()
        return render(request, "list_view.html", context)

def list_user_from_promotion(request):
    user = request.user.id
    context = {}
    context["promotions"] =  Promotion.objects.filter(user__id=user)
    return render(request, "list_view.html", context)


def detail_user(request, id):
    if not is_authorized(request, "view", 'users'):
        return redirect('/accounts/login/')
    else:
        context = {}
        context["data"] = User.objects.get(id=id)
        return render(request, "detail_view.html", context)


def update_user(request, id):
    if not is_authorized(request, "change", 'users'):
        return redirect('/accounts/login/')
    else:
        context = {}
        obj = get_object_or_404(User, id=id)
        form = CustomUserCreationForm(request.POST or None, instance=obj)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("../")

        context["form"] = form

        return render(request, "update_view.html", context)


def delete_user(request, id):
    if not is_authorized(request, "delete", 'users'):
        return redirect('/accounts/login/')
    else:
        context = {}
        obj = get_object_or_404(User, id=id)
        if request.method == "POST":
            obj.delete()
            return HttpResponseRedirect("../")

        return render(request, "delete_view.html", context)

def teacher_dashboard(request):
    user = request.user
    subjects = Subject.objects.filter(user_id= user.id)
    return render(request, '../templates/teacher/index.html', {'subjects': subjects})
